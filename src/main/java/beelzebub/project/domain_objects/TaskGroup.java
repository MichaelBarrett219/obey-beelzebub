/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.domain_objects;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author kenji
 */

import java.util.Random;
@Entity
public class TaskGroup {
    
    @Column
    @ElementCollection
    private List<Task> tasks;
    
    @OneToOne
    private User createdBy;
    private String name;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    public TaskGroup() {
        tasks = new ArrayList<>();
    }
    
    public TaskGroup(User createdBy) {
        this.createdBy = createdBy;
        this.name = "New Task Group";
        tasks = new ArrayList<>();
    }
    
    public TaskGroup(String name, User createdBy) {
        this.name = name;
        this.createdBy = createdBy;
        Random rand = new Random();
    }
    
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public User getCreatedBy() {
        return this.createdBy;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void addTask(Task task) {
        this.tasks.add(task);
    }
}
