/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.domain_objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author mike
 */

@Entity
public class Task {
    private String name;
    
    @OneToOne
    private TaskGroup belongsTo;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    public Task() {}
    
    public Task(TaskGroup belongsTo) {
        name = "New Task";
        this.belongsTo = belongsTo;
    }
    
    public Task(String name) {
        this.name = name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public TaskGroup getBelongsTo() {
        return this.belongsTo;
    }
    
    public int getId() {
        return this.id;
    }
}
