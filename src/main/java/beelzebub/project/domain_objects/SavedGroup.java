/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.domain_objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author kenji
 */

@Entity
public class SavedGroup {

    @OneToOne
    private User user;
    @OneToOne
    private TaskGroup group;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    public SavedGroup() {}
    
    public SavedGroup(User user, TaskGroup group) {
        this.user = user;
        this.group = group;
    }
    
    public User getUser() {
        return user;
    }
    public TaskGroup getGroup() {
        return group;
    }
}
