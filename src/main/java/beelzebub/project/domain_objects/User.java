/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.domain_objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;


/**
 *
 * @author kdill
 */
@Entity
public class User {
    
    @Column
    @ElementCollection
    private List<TaskGroup> taskGroups;

    private String name;
    private String email;
    private String password;
    
    @Transient
    private String confirmPassword;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    public User() {
        taskGroups = new ArrayList<>();
    }
    
    @JsonCreator
    public User (@JsonProperty("name") String name, @JsonProperty("email") String email, @JsonProperty("password") String password){
        this.name=name;
        this.email=email;
        this.password=password;
        taskGroups = new ArrayList<>();
    }
    
    @Override
    public boolean equals(Object o){
        if (!(o instanceof User))
            return false;
        return ((User) o).id == this.id;
    }
    
    @Override
    public int hashCode(){
        return (int)id;
    }

    public String getName() {
    	return name;
    }

    public String getPassword() {
        return password;
    }
    
    public int getUserID() {
    	return id;
    }
    
    public String getEmail() {
    	return email;
    }
    
    public void setEmail(String email) {
    	this.email = email;
    }
    
    public void setPassword(String password) {
	this.password = password;
    }
	
    public String getConfirmPassword() {
    	return confirmPassword;
    }
    
    public List<TaskGroup> getTaskGroups() {
        return this.taskGroups;
    }
    
    public void addTaskGroup(TaskGroup group) {
        this.taskGroups.add(group);
    }
    
}

