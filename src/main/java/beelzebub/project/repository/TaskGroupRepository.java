/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.repository;

import beelzebub.project.domain_objects.TaskGroup;
import beelzebub.project.domain_objects.User;
import java.util.List;
import org.aspectj.weaver.patterns.TypePatternQuestions.Question;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TaskGroupRepository extends JpaRepository<TaskGroup, Integer> {
    
    TaskGroup findByName(String name);
	
    TaskGroup findById(int id);
    
    List<TaskGroup> findByCreatedBy(User user);
    
    @Query("SELECT tg.id FROM TaskGroup tg ORDER BY RAND()")
    List<Integer> getRandomTaskGroup();

    public TaskGroup findById(TaskGroup get);
}
