/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.repository;

import beelzebub.project.domain_objects.User;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
	
    List<User> findByNameIgnoreCaseContaining(String name);
	
    User findByName(String name);
	
    User findById(int id);
    
    User findByEmail(String email);

    boolean existsById(int id);
    
}
