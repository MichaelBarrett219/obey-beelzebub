/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.repository;

import beelzebub.project.domain_objects.Task;
import beelzebub.project.domain_objects.TaskGroup;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    
    Task findByName(String name);
	
    Task findById(int id);
    
    List<Task> findByBelongsTo(TaskGroup group);
}
