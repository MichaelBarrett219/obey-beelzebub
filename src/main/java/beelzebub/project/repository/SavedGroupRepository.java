/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.repository;

import beelzebub.project.domain_objects.SavedGroup;
import beelzebub.project.domain_objects.TaskGroup;
import beelzebub.project.domain_objects.User;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SavedGroupRepository extends JpaRepository<SavedGroup, Integer> {
    List<SavedGroup> findByUser(User user);
    SavedGroup findByUserAndGroup(User user, TaskGroup group);
}
