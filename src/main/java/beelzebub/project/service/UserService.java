/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.service;

import beelzebub.project.domain_objects.PasswordAuthentication;
import beelzebub.project.domain_objects.User;
import beelzebub.project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PasswordAuthentication passwordAuthentication;
	
	public void save(User user) {
		user.setPassword(passwordAuthentication.hash(user.getPassword().toCharArray()));
		userRepository.save(user);
	}
	
	public void changePassword(User user, String pass) {
		user.setPassword(passwordAuthentication.hash(pass.toCharArray()));
		userRepository.save(user);
	}
	
	public boolean authenticate(String username, String password) {
//		System.out.println(username);
//		System.out.println(password);
//		System.out.println(findByUsername(username).getPassword());
		String hashToCompare = findByUsername(username).getPassword();
		return passwordAuthentication.authenticate(password.toCharArray(), hashToCompare);
	}
	
	public User findByUsername(String name) {
		return userRepository.findByName(name);
	}
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	public User findById(int id) {
		return userRepository.findById(id);
	}
}
