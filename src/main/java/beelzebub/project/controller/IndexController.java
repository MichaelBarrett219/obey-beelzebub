/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.controller;

import beelzebub.project.domain_objects.SavedGroup;
import beelzebub.project.domain_objects.Task;
import beelzebub.project.domain_objects.TaskGroup;
import beelzebub.project.domain_objects.User;
import beelzebub.project.repository.SavedGroupRepository;
import beelzebub.project.repository.TaskGroupRepository;
import beelzebub.project.repository.TaskRepository;
import beelzebub.project.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class IndexController {
    
        @Autowired
	private UserRepository userRepository;
        
        @Autowired
	private TaskGroupRepository taskGroupRepository;
        
        @Autowired
        private SavedGroupRepository savedGroupRepository;
        
        @Autowired
	private TaskRepository taskRepository;
        
        @Autowired
        private HttpSession session;
        
	@RequestMapping("/")
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");	
                User u = (User) session.getAttribute("user");
                if (u != null) {
                    List<TaskGroup> groups = taskGroupRepository.findByCreatedBy(u);
                    List<SavedGroup> saved = savedGroupRepository.findByUser(u);
                    for(SavedGroup save : saved) {
                        groups.add(save.getGroup());
                    }
                    mav.addObject("taskGroupList", groups);
                }
                else System.out.println("Error");
		return mav;
	}
        
        @RequestMapping("/tasks/{taskGroupID}")
	public ModelAndView tasks(@PathVariable int taskGroupID) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("tasks");	
                User u = (User) session.getAttribute("user");
                TaskGroup group = taskGroupRepository.findById(taskGroupID);
                List<Task> tasks = taskRepository.findByBelongsTo(group);
		if (u != null) mav.addObject("taskList", tasks);
                mav.addObject("taskGroupID", taskGroupID);
                mav.addObject("taskGroupName", group.getName());
                mav.addObject("ownerID", group.getCreatedBy().getUserID());
                mav.addObject("ownerName", group.getCreatedBy().getName());
		return mav;
	}
        
        @RequestMapping("/users/{userID}")
	public ModelAndView users(@PathVariable int userID) {
                ModelAndView mav = new ModelAndView();
		mav.setViewName("users");	
                User u = userRepository.findById(userID);
                List<TaskGroup> groups = taskGroupRepository.findByCreatedBy(u);         
                List<SavedGroup> saved = savedGroupRepository.findByUser(u);
                
                for(SavedGroup save : saved) {
                    groups.add(save.getGroup());
                }
                
		if (u != null) mav.addObject("taskGroupList", groups);
                mav.addObject("userID", u.getUserID());
                mav.addObject("userName", u.getName());
                
                int[] saves = new int[groups.size()];
                User sesh = (User) session.getAttribute("user");
                for(int i=0; i<groups.size();i++) {
                    if(savedGroupRepository.findByUserAndGroup(sesh, groups.get(i)) != null) saves[i] = 1;
                    else saves[i] = 0;
                }
                
                mav.addObject("saves", saves);

                //else System.out.println("Error");
		return mav;
	}
        
        /*@RequestMapping("/error")
	public ModelAndView error() {
                ModelAndView mav = new ModelAndView();
		mav.setViewName("error");	
		return mav;
	}*/
        
        @RequestMapping("/discover")
	public ModelAndView discover() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("discover");	
                //User u = (User) session.getAttribute("user");
                
                List<Integer> group = taskGroupRepository.getRandomTaskGroup();
                Optional<TaskGroup> fin = taskGroupRepository.findById(group.get(0));
                mav.addObject("taskgroup", fin.get());
                
                User u = (User) session.getAttribute("user");
                TaskGroup tg = fin.get();
                List<Task> tasks = taskRepository.findByBelongsTo(tg);
		if (u != null) mav.addObject("taskList", tasks);
                mav.addObject("taskGroupID", tg.getId());
                mav.addObject("taskGroupName", tg.getName());
                mav.addObject("ownerID", tg.getCreatedBy().getUserID());
                mav.addObject("ownerName", tg.getCreatedBy().getName());
		return mav;
	}
}
