/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beelzebub.project.controller;

import beelzebub.project.domain_objects.PasswordAuthentication;
import beelzebub.project.domain_objects.SavedGroup;
import beelzebub.project.domain_objects.Task;
import beelzebub.project.domain_objects.TaskGroup;
import beelzebub.project.domain_objects.User;
import beelzebub.project.repository.SavedGroupRepository;
import beelzebub.project.repository.TaskGroupRepository;
import beelzebub.project.repository.TaskRepository;
import beelzebub.project.repository.UserRepository;
import beelzebub.project.service.UserService;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
        
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository userRepository;
        
        @Autowired
        private SavedGroupRepository savedGroupRepository;
        
        @Autowired
	private TaskGroupRepository taskGroupRepository;
        
        @Autowired
	private TaskRepository taskRepository;
	
	@Autowired
	private PasswordAuthentication passwordAuthentication;
        
    @Autowired
    private HttpSession session;
    
	@PostMapping("/register")
	public ResponseEntity<?> registerRequest(@Valid @RequestBody User user, Errors errors, HttpSession session) {
		Map<String, String> response = new HashMap<String, String>();
		System.out.println(user.getName());
		if (errors.hasErrors()) {
			response.put("error", "ajax register request failed");
			return ResponseEntity.badRequest().body(response);
                }
		
		
		if (userService.findByUsername(user.getName()) != null) {
			response.put("success", "false");
			response.put("reason", "username");
		} 
		else if (userService.findByEmail(user.getEmail()) != null) {
			response.put("success", "false");
			response.put("reason", "email");
		}
		else if (!user.getPassword().equals(user.getConfirmPassword())) {
			response.put("success", "false");
			response.put("reason", "password");
		}
		else {
			response.put("success", "true");
			userService.save(user);
			session.setAttribute("user", user);
		}

		return ResponseEntity.ok(response);
	}
   
        
	@PostMapping("/login")
	public ResponseEntity<?> loginRequest(@Valid @RequestBody User user, Errors errors, HttpSession session) {
		Map<String,String> response = new HashMap<String, String>();
		
		if (errors.hasErrors()) {
			response.put("error", "ajax login request failed");
			return ResponseEntity.badRequest().body(response);
		}
		
		User targetUser = userService.findByUsername(user.getName());
		if (targetUser != null) {
			if (userService.authenticate(user.getName(), user.getPassword())) {
				response.put("success", "true");
				session.setAttribute("user", targetUser);
			}
			else {
				response.put("success", "false");
				response.put("reason", "password");
			}
		}
		else {
			response.put("success", "false");
			response.put("reason", "username");
		}
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/logout")
	public ResponseEntity<?> logoutRequest(HttpSession session) {
		Map<String, String> response = new HashMap<String, String>();
		try {
			session.invalidate();
		}
		catch (Exception e) {}
		
		response.put("success", "true");
		return ResponseEntity.ok(response);
	}
        
        @PostMapping("/newgroup/{userID}")
	public ResponseEntity<?> createNewgroup(@PathVariable int userID) {
                Map<String, String> response = new HashMap<String, String>();
                User user = userRepository.findById(userID);
                TaskGroup taskToAdd = new TaskGroup(user);
                taskGroupRepository.save(taskToAdd);
                user.addTaskGroup(taskToAdd);
                userRepository.save(user);
                response.put("success", "true");
                return ResponseEntity.ok(response);
        }
        
        @DeleteMapping("/deletegroup/{groupID}")
	public ResponseEntity<?> deleteGroup(@PathVariable int groupID) {
                Map<String, String> response = new HashMap<String, String>();
                taskGroupRepository.delete(taskGroupRepository.findById(groupID));
                response.put("success", "true");
                return ResponseEntity.ok(response);
        }
        
        @PostMapping("/newtask/{taskGroupID}")
	public ResponseEntity<?> createNewTask(@PathVariable int taskGroupID) {
                Map<String, String> response = new HashMap<String, String>();
                TaskGroup group = taskGroupRepository.findById(taskGroupID);
                Task taskToAdd = new Task(group);
                taskRepository.save(taskToAdd);
                group.addTask(taskToAdd);
                taskGroupRepository.save(group);
                response.put("success", "true");
                return ResponseEntity.ok(response);
        }
        
        @DeleteMapping("/deletetask/{taskID}")
	public ResponseEntity<?> deleteTask(@PathVariable int taskID) {
                Map<String, String> response = new HashMap<String, String>();
                taskRepository.delete(taskRepository.findById(taskID));
                response.put("success", "true");
                return ResponseEntity.ok(response);
        }
        
        @PostMapping("/savegroup/{taskGroupID}")
	public ResponseEntity<?> saveTask(@PathVariable int taskGroupID) {
                Map<String, String> response = new HashMap<String, String>();
                TaskGroup group = taskGroupRepository.findById(taskGroupID);
                User u = (User)session.getAttribute("user");
                SavedGroup potentialGroup = savedGroupRepository.findByUserAndGroup(u, group);
                if(potentialGroup == null) savedGroupRepository.save(new SavedGroup(u,group));
                response.put("success", "true");
                return ResponseEntity.ok(response);
        }
        
        @DeleteMapping("/unsavegroup/{taskGroupID}")
	public ResponseEntity<?> unsaveTask(@PathVariable int taskGroupID) {
                Map<String, String> response = new HashMap<String, String>();
                User u = (User)session.getAttribute("user");
                savedGroupRepository.delete(savedGroupRepository.findByUserAndGroup(u, taskGroupRepository.findById(taskGroupID)));
                response.put("success", "true");
                return ResponseEntity.ok(response);
        }
        
        @PostMapping("/changegroupname/{taskGroupID}")
	public ResponseEntity<?> changeGroupName(@PathVariable int taskGroupID, @Valid @RequestBody String name) {
                Map<String, String> response = new HashMap<String, String>();
                TaskGroup group = taskGroupRepository.findById(taskGroupID);
                group.setName(name);
                taskGroupRepository.save(group);
                response.put("success", "true");
                return ResponseEntity.ok(response);
        }
        
        @PostMapping("/changetaskname/{taskID}")
	public ResponseEntity<?> changeTaskName(@PathVariable int taskID, @Valid @RequestBody String name) {
                Map<String, String> response = new HashMap<String, String>();
                Task task = taskRepository.findById(taskID);
                task.setName(name);
                taskRepository.save(task);
                response.put("success", "true");
                return ResponseEntity.ok(response);
        }
}
