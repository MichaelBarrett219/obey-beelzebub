function invalidUsername() {
    $('.username-input').addClass('is-danger');
    $('.invalid-username').removeClass('is-hidden');
}

function invalidEmail() {
    $('.email-input').addClass('is-danger');
    $('.invalid-email').removeClass('is-hidden');
}

function invalidPassword() {
    $('.password-input').addClass('is-danger');
    $('.invalid-password').removeClass('is-hidden');
}

function invalidPassword2() {
    $('.password2-input').addClass('is-danger');
    $('.invalid-password2').removeClass('is-hidden');
}

$('.open-auth').click(function() {
    $('.auth-modal').addClass("is-active");
});

$('.close-auth').click(function() {
    $('.auth-modal').removeClass("is-active");
});

$('.open-reg').click(function() {
    $('.reg-modal').addClass("is-active");
});

$('.close-reg').click(function() {
    $('.reg-modal').removeClass("is-active");
});

$('.login-button').click(function() {
    
	var User = new Object();
	User.name = $('.username-input').val();
	User.password = $('.password-input').val();
        
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
        
	$.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "POST",
		url: "/login",
		data: JSON.stringify(User),
		dataType: 'json',
		cache: false,
		timeout: 600000,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			console.log(data);
			if (data["success"] === "true") {
				window.location.reload();
			}
			else {
				if (data["reason"] === "username") {
					console.log("Incorrect username");
				}
				else if (data["reason"] === "password") {
					console.log("Incorrect password");
				}
                                else if (data["reason"] === "email") {
					console.log("Incorrect email");
				}
			}
		},
		error: function(e) {
			//alert(e.responseText);
		}
	});
});

$('.register-button').click(function() {
	var User = new Object();
	User.name = $('.username-reg-input').val();
        User.email = $('.email-reg-input').val();
	User.password = $('.password-reg-input').val();
        User.confirmPassword = $('.password2-reg-input').val();
        
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "POST",
		url: "/register",
		data: JSON.stringify(User),
		dataType: 'json',
		cache: false,
		timeout: 600000,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			console.log(data);
			if (data["success"] === "true") {
				window.location.reload();
			}
			else {
				if (data["reason"] === "username") {
					console.log("Username is taken");
				}
				else if (data["reason"] === "email") {
					console.log("Email is taken");
				}
				else if (data["reason"] === "password") {
					console.log("Password and confirm password don't match");
				}
			}
		},
		error: function(e) {
			alert(e.responseText);
		}
	});
});

$('.logout-button').click(function() {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "GET",
		url: "/logout",
		cache: false,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			window.location.reload();
		},
		error: function(e) {
			//alert(e.responseText);
		}
	});
});

$('.username-input, .password-input').on('keyup', function (e) {
    if (e.keyCode == 13) {
        $('.login-button').trigger( "click" );
    }
});