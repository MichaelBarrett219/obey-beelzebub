$('.add-group').click(function() {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
        
    $.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "POST",
		url: "/newgroup/" + $("#userID").val(),
		cache: false,
		timeout: 600000,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			console.log(data);
			if (data["success"] === "true") {
				window.location.reload();
			}
		},
		error: function(e) {
			//alert(e.responseText);
		}
	});
});

$('.delete-group').click(function() {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    
    $.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "DELETE",
		url: "/deletegroup/" + this.id,
		cache: false,
		timeout: 600000,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			console.log(data);
			if (data["success"] === "true") {
				window.location.reload();
			}
		},
		error: function(e) {
			//alert(e.responseText);
		}
	});
});

$('.clickable').click(function() {
    window.location = $(this).data("href");
});


$('.add-task').click(function() {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
        
    $.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "POST",
		url: "/newtask/" + $("#taskGroupID").val(),
		cache: false,
		timeout: 600000,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			console.log(data);
			if (data["success"] === "true") {
				window.location.reload();
			}
		},
		error: function(e) {
			//alert(e.responseText);
		}
	});
});

$('.delete-task').click(function() {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    
    $.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "DELETE",
		url: "/deletetask/" + this.id,
		cache: false,
		timeout: 600000,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			console.log(data);
			if (data["success"] === "true") {
				window.location.reload();
			}
		},
		error: function(e) {
			//alert(e.responseText);
		}
	});
});

$('.save-group').click(function() {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    
    $.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "POST",
		url: "/savegroup/" + this.id,
		cache: false,
		timeout: 600000,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			console.log(data);
			if (data["success"] === "true") {
				window.location.reload();
			}
		},
		error: function(e) {
			//alert(e.responseText);
		}
	});
});

$('.unsave-group').click(function() {
    //alert(this.id)
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    
    $.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "DELETE",
		url: "/unsavegroup/" + this.id,
		cache: false,
		timeout: 600000,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			console.log(data);
			if (data["success"] === "true") {
				window.location.reload();
			}
		},
		error: function(e) {
			//alert(e.responseText);
		}
	});
});

$('.edit-button').click(function(){
    $('input#'+this.id+'.edit-input').removeClass('is-hidden');
    $('a#'+this.id+'.clickable').addClass('is-hidden');
});

$('input.edit-input').on('keyup', function (e) {
    if (e.keyCode == 13) {
       $('a#'+this.id+'.clickable').text( $('input#'+this.id+'.edit-input').val());
       $('a#'+this.id+'.clickable').removeClass('is-hidden');
       $('input#'+this.id+'.edit-input').addClass('is-hidden');  
       
       
       
        var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
        var name = $('input#'+this.id+'.edit-input').val()
	$.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "POST",
		url: "/changegroupname/"+this.id,
		data: name,
		dataType: 'json',
		cache: false,
		timeout: 600000,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			console.log(data);
			if (data["success"] === "true") {
			}
			else {
			}
		},
		error: function(e) {
			//alert(e.responseText);
		}
	});
    }
});

$('.edit-task-button').click(function(){
    $('input#'+this.id+'.edit-task-input').removeClass('is-hidden');
    $('span#'+this.id+'.task').addClass('is-hidden');
});

$('input.edit-task-input').on('keyup', function (e) {
    if (e.keyCode == 13) {
       $('span#'+this.id+'.task').text( $('input#'+this.id+'.edit-task-input').val());
       $('span#'+this.id+'.task').removeClass('is-hidden');
       $('input#'+this.id+'.edit-task-input').addClass('is-hidden');  
       
        var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
        var name = $('input#'+this.id+'.edit-task-input').val();
	$.ajax({
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		type: "POST",
		url: "/changetaskname/"+this.id,
		data: name,
		dataType: 'json',
		cache: false,
		timeout: 600000,
		beforeSend: function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function(data) {
			console.log(data);
			if (data["success"] === "true") {
			}
			else {
			}
		},
		error: function(e) {
			//alert(e.responseText);
		}
	});
    }
});

$('.refresh').click(function() {
    window.location = '/discover';
});